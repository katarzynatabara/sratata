<?php
function sratata_customize_register($wp_customize) {
	

	$wp_customize->add_section( 'sratata' , array(
    		'title'      => __( 'Podtytul', 'sratata' ),
    		'priority'   => 30,
		)
	);

	$wp_customize->add_setting( 'header_text_color' , array(
			'default'   => '#FFFFFF',
    		'transport' => 'postMessage',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
			'label'      => __( 'Kolor podtytulu', 'sratata' ),
			'section'    => 'sratata',
			'settings'   => 'header_text_color',
			)
		)
	);

		$wp_customize->add_setting( 'subtitle' , array(
			'default'   => 'Podaj drugi tytul',
    		'transport' => 'postMessage',
    		
		)
	);
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'subtitle', array(
			'label'      => __( 'Drugi tytul', 'sratata' ),
			'section'    => 'sratata',
			'settings'   => 'subtitle',
			'type'		 => 'text',
			)
		)
	);




	function sratata_customize_css()
	{
    	?>
        	 <style type="text/css">
        	     h1 { color: <?php echo get_theme_mod('header_color', '#FFFFFF'); ?>; }
       		 </style>
    	<?php
	}
	add_action( 'wp_head', 'sratata_customize_css');
}
add_action( 'customize_register', 'sratata_customize_register');

	// function sratata_customfield() {
	// 	if ( is_single()) {
	// 		$field = get_post_meta(get_the_ID(), 'custome_field', true);
	// 		echo '<div class="new-class">'.$field. '</div>';
	// 	}

	// }
	// add_action('loop_start','sratata_customfield');
?>